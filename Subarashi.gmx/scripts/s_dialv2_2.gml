message_position((window_get_x()+156),(window_get_y()+236))
message_background(bg_txtfairy)
show_message("Good! You learn fast.")
show_message("If you want to see which explosions you can use, look at the upper-right corner of the screen!")
message_background(bg_txtninja)
show_message("Good thing you teamed up with me!")
show_message("And now for some crystal hunting!")
show_message("Let's go!")
