message_position((window_get_x()+156),(window_get_y()+236))
message_background(bg_txtninja)
show_message("Seems I have to get rid of this lava before passing.")
message_background(bg_txtfairy)
show_message("That's right! You'll have to break the rocks below the lava before passing.")
show_message("And then jump over the lava pool!")
show_message("How does that feel, jumping over a pool of lava hoping you timed your jump right?")
message_background(bg_txtninja)
show_message("You're getting very sweaty. And yes, that's because of two reasons.")
