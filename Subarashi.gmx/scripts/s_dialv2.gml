message_position((window_get_x()+156),(window_get_y()+236))
message_background(bg_txtfairy)
show_message("I will now instruct you how to use my power.")
show_message("Listen well.")
show_message("You will see a blast radius appear at your mouse cursor.")
show_message("When clicking the left mouse button, you will destroy sand and rocks in the radius.")
message_background(bg_txtninja)
show_message("What is the difference between rocks and sand?")
message_background(bg_txtfairy)
show_message("Sand will fall down, but a rock will stay on its place.")
show_message("There are also these iron blocks, but I don't have the might to destroy these.")
message_background(bg_txtninja)
show_message("Well, how do I get started?")
message_background(bg_txtfairy)
show_message("Do you see those rocks there?")
show_message("Get the radius on the rocks and click!")
