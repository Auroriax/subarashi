message_position((window_get_x()+156),(window_get_y()+236))
if show_question("Do you want to log in to GameJolt? [Select No if you don't know what GameJolt is.]") = true
    { uname = get_string("Okay! Now, enter your username.","")
      utoken = get_string("And what's your usertoken?","")
      if GJ_login(uname,utoken) = 1
        { show_message("You got it! You succesfully logged in as " + uname + ".") 
        show_message("Achievement pickups are activated. If you see a white token with a symbol,")
        show_message("you can completly optionally obtain it for an trophy. Enjoy!")
        global.usernme = GJ_getLoggedData("username")}
        else
        { show_message("Login failure. Please try logging in again.") 
        s_login()}
}
