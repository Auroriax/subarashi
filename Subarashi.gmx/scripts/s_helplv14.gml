message_position((window_get_x()+156),(window_get_y()+236))
message_background(bg_txtninja)
show_message("I don't see any crystals here!")
message_background(bg_txtfairy)
show_message("That's right, they're all in that big grey box.")
message_background(bg_txtninja)
show_message("Well, how am I supposed to get them?")
message_background(bg_txtfairy)
show_message("It are all purple crystals, so you can get them by destroying them.")
message_background(bg_txtninja)
show_message("Ah, okay! Any more hints?")
message_background(bg_txtfairy)
show_message("Try to collect 40 crystals with your first explosion. That's the best first move.")
