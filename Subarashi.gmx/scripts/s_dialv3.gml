message_position((window_get_x()+156),(window_get_y()+236))
var xx;
message_background(bg_txtfairy)
show_message("This should be easy now you know how my power works.")
message_background(bg_txtninja)
show_message("I see some crystals over there, and some sand too...")
message_background(bg_txtfairy)
show_message("There are also crystals under the sand.")
xx = show_question("Do you want to get a hint how to get those?")
if xx = 1
{ show_message("You'll have to destroy the rocks so the sand falls down.")
message_background(bg_txtninja)
show_message("Ah, I see. I'll go get them, then!")
}
else
{ message_background(bg_txtninja)
show_message("No, thank you. I'd rather find out myself.")
message_background(bg_txtfairy)
show_message("Very well, then. Good luck.")
}
message_background(bg_txtfairy)
show_message("And if you're stuck, press [R] to restart the level.")
