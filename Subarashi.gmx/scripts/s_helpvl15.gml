message_position((window_get_x()+156),(window_get_y()+236))
message_background(bg_txtfairy)
show_message("Hmm...")
show_message("I think you will need my last explosion for this one.")
message_background(bg_txtninja)
show_message("Wait, you can only cast FIVE different explosions?")
message_background(bg_txtfairy)
show_message("Speak for yourself! You can't even cast one explosion!")
message_background(bg_txtninja)
show_message("...")
show_message("Awtsch.")
message_background(bg_txtfairy)
show_message("Anyway, you'd better use this to dig your way to the crystals.")
show_message("Plan your way through!")
